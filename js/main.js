$(document).ready(function() {
    addAnimate(".animated");
    $(window).scroll(function() {
        addAnimate(".animated");
        changeCirclePercent();

    });
    setInterval(() => {
        changeLinearPercent();
    }, 800);
    $("#anchor").on("click", function(event) {
        event.preventDefault();
        let id = $(this).attr('href');
        let top = $(id).offset().top;
        $('body,html').animate({ scrollTop: top }, 1000);
    });

    let buttonUp = $('.batton_top');
    buttonUp.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 2000);
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() > $(window).height()) {
            if (buttonUp.hasClass("visible")) return;
            buttonUp.addClass('visible');
        } else {
            buttonUp.removeClass('visible');
        }
    });

    function addAnimate(selector) {
        $(`${selector}`).each(function() {
            let elPos = $(this).offset().top;
            let topOfWindow = $(window).scrollTop();
            if (elPos < topOfWindow + $(window).height() + 100) {
                if ($(this).hasClass("fromLeft")) {
                    $(this).addClass("fadeInLeft removeOpacity");
                } else if ($(this).hasClass("fromTop")) {
                    $(this).addClass("fadeInDown removeOpacity");
                } else if ($(this).hasClass("fromRight")) {
                    $(this).addClass("fadeInRight removeOpacity");
                } else if ($(this).hasClass("fromBottom")) {
                    $(this).addClass("fadeInUp removeOpacity");
                }
            }
        })
    }

    function isVisible(selector) {
        let element = $(selector);
        let elementVerticalPosition = $(element).offset().top;
        let elementHorizontalPosition = $(element).offset().left;
        let topOfWindow = $(window).scrollTop();
        return ((elementVerticalPosition < topOfWindow + $(window).height() + 100) && (elementHorizontalPosition > 0 && elementHorizontalPosition < 1170));
    }

    function changeCirclePercent() {
        if (!(isVisible(".c100"))) return;
        let circles = $(".c100");
        if (!circles.hasClass("p0")) return;
        circles.each(function() {
            let targetPercent = $(this).attr("data-value");
            for (let i = 0; i <= targetPercent; i++) {
                setTimeout(() => {
                    $(this).children("span").html(`${i}<span>%</span>`);
                    $(this).removeClass();
                    $(this).addClass(`c100 ${"p" + i} green`);
                }, 75 * i)
            }
        })
    }

    function changeLinearPercent() {
        let slides = $(".slider");
        slides.each(function() {
            if (!isVisible(this)) return;
            let lines = $(this).find(".skill_container");
            if ($(".value", lines)[0].innerText !== "0 %") return;
            lines.each(function() {
                let targetPercent = $(this).attr("data-value");
                for (let i = 0; i <= targetPercent; i++) {
                    setTimeout(() => {
                        $(".value", this)[0].innerText = `${i} %`;
                        $(".skill_bar", this).css("width", `${i}%`);
                    }, 45 * i)
                }
            });
        });
    }
    $('#slowlyCarousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: false,
        loop: true,

        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
                autoplayHoverPause: true,
                autoplay: true,
                autoplayTimeout: 5000,
                autoplaySpeed: 2500,
            }
        }
    })
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        nav: false,
        loop: true,

        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 1,
            },
            1000: {
                items: 1,
                autoplayHoverPause: true,
                autoplay: true,
                autoplayTimeout: 3500,
                autoplaySpeed: 1500,
            }
        }
    })
});